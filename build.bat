nuget restore Perceive.sln
xcopy C:\Percive\packages\Ninject.3.2.2.0\lib\net45-full\*.* C:\Percive\Perceive.Application.Web\bin\ /T
xcopy C:\Percive\packages\Ninject.3.2.2.0\lib\net45-full\*.* C:\Percive\Perceive.Application.Web\bin\ 
msbuild LeafSoftwareSolutions.Infrastructure\LeafSoftwareSolutions.Infrastructure.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Core\Perceive.Core.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.ServiceBus.Core\Perceive.ServiceBus.Core.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild LeafSoftwareSolutions.ServiceBus\LeafSoftwareSolutions.ServiceBus.fsproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Data\Perceive.Data.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Domain\Perceive.Domain.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Dependencies\Leadtools.Dependencies\Leadtools.Dependencies.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.ServiceBus\Perceive.ServiceBus.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Data.NHibernate\Perceive.Data.NHibernate.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Imaging.LeadTools.Barcode\Perceive.Imaging.LeadTools.Barcode.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.DependencyInjection.Ninject\Perceive.DependencyInjection.Ninject.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Api.Client\Perceive.Api.Client.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Imaging.LeadTools\Perceive.Imaging.LeadTools.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.DependencyInjection.Ninject.Service\Perceive.DependencyInjection.Ninject.Service.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Tests\Perceive.Test.Common\Perceive.Test.Common.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.ScanService\Perceive.ScanService.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Application\Perceive.Application.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Tests\Perceive.IntegrationTests\Perceive.IntegrationTests.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Database\TMSStub\TMSStub.sqlproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.DependencyInjection.Ninject.Web\Perceive.DependencyInjection.Ninject.Web.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Database\KofaxReleaseDB\KofaxReleaseDBStub.sqlproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Database\RetrieveDbStub\RetrieveDbStub.sqlproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
msbuild Perceive.Application.Web\Perceive.Application.Web.csproj /property:Configuration=debug /property:Platform=x64 /property:SolutionDir=C:\Percive\ /t:rebuild
